# GUSTAV

All-in-one, feature rich, docker based development environment for **silverstripe** and **react**
with a "strange" name due to lack of creativity :D Got catchy ideas?

| Includes    |                   |              |
|-------------|-------------------|--------------|
| apache 2.4  | php-fpm 5.x - 8.x | mariadb 10.7 |
| node + yarn | phpmyadmin        | mailhog      |

Feel free to modify everything to fit your needs.

## Requirements

- Linux OS (Ubuntu / Debian / WSL2 + Docker Desktop)
- Docker and ``docker compose`` installed
- git installed

## Set Up and start

1. fork gustav to create a new git repository/project
2. ``cp .env.example .env``
3. modify .env file according to your needs
4. ``chmod +x gustav``
5. ``./gustav setup-silverstripe``
6. ``./gustav setup-react``
7. ``./gustav up`` or ``./gustav up -d``
8. spin up your ide and browser to have fun

### Environment files

The hole repository has one env file and the silverstripe installation has its own. Both have a ``.env.example`` file to
get you started. **Required** values are marked and each variable has a short explanation.

## Serve react app with https

Change the start command for the node container inside the ``docker-compose.yml`` file from
```command: sh -c 'yarn start'```
to
```command: sh -c 'yarn start-https'```

### Access deployed containers

**Browser**

- silverstripe: ``http://localhost`` or secured ``https://localhost``
- react: ``http://localhost:3000`` or secured  ``https://localhost:3000``
- phpmyadmin: ``http://localhost:8024``
- mailhog: ``http://localhost:8025``

You can modify the ports inside the ``.env`` file

**Container**

Have a look at the **Helper Script** section

### Helper Script

To simplify the work with the containers, there is a helper script called "gustav.
To use it, you have to make it executable first ``chmod +x gustav``.

#### Up, Down & Logs

To start or stop the environment use ``./gustav up`` or ``./gustav down``. These are only wrappers for ``docker compose``
So for example you can attach ``-d`` to start in detached mode or ``-v`` to shut down and delete volumes.

If you only want to start what is required for Silverstripe use ``./gustav up silverstripe``
or for React use ``./gustav up react``. Same applies for ``./gustav down``

To view logs of running containers simply use ``./gustav logs``. You can append a single container name to view only those
``./gustav logs apache``

#### Build Tools and Package Managers

You can use these tools as you are used to, you just have to "prefix" them with ``./gustav``

- ``./gustav composer``
- ``./gustav sake``
- ``./gustav yarn`` this will execute for the react app
- ``./gustav npm`` this will execute for the react app
- ``./gustav yarn-ss`` this will execute for silverstripe
- ``./gustav npm-ss`` this will execute for silverstripe

#### Interactive Shell

You can also open an interactive shell for a container by typing ``./gustav shell-for CONTAINER_NAME``.

If you want to use the shell as root user, you only have to add ``as-root``.
So for example ``./gustav shell-for apache as-root``.

#### Execute a single command  

Likewise, individual commands within the container can be executed using the command

``./gustav exec-in CONTAINER_NAME COMMAND``

to execute it as root, append ``as-root`` after the container name.
So for example ``./gustav exec-in node as-root yarn install ``

#### Restart containers

``./gustav restart CONTAINER_NAME`` to restart a single container

#### Silverstripe Helpers

- export database with: ``./gustav db-export``
- import database with: ``./gustav db-import filename.sql``
- export silverstripe assets with: ``./gustav assets-export``
- import silverstripe assets with: ``./gustav assets-import assets.zip`` Zip needs to contain "public/assets" folder structure

#### Project update / Project install

To update your project to the most recent version(s) run ``./gustav project-update`` that will perform the following

1. git pull
2. composer update
3. sake dev/build "flush=all"
4. yarn upgrade
5. yarn-ss upgrade

to only fetch the latest commits from git and perform a ``composer install`` + ``yarn install`` run ``./gustav project-install`` 

## Deployment

Deployment is done with GitLabs CI/CD pipelines. It will be triggered automatically,
after you pushed to the production or staging branch.

To make this work, follow the steps below!

### Configure GitLab

1. execute ``./gustav get-composer-auth`` and copy the output
2. Go to your GitLab repository page and navigate to "Settings > CI/CD > Variables". This can also be done globally inside your groups to prevent to do it over and over again for each project
3. Add a new variable (Type = Variable, Env = All, Flags = none) named ``COMPOSER_AUTH`` and paste the former copied output
4. Add a new variable (Type = Variable, Env = All, Flags = none) named ``DEPLOY_SSH_PRIVATE_KEY`` and paste a ssh key inside that has access to your target server
5. Add a new variable (Type = File, Env = All, Flags = none) named ``DOTENV_PRODUCTION`` and paste in the contents of the silverstripe .env file for production environment
6. Add a new variable (Type = File, Env = All, Flags = none) named ``DOTENV_STAGING`` and paste in the contents of the silverstripe .env file for staging environment

### Create Docker Image for GitLab CI/CD

1. create an access token with ``read_registry`` and ``ẁrite_registry`` scopes ind [GitLab](https://gitlab.com/-/profile/personal_access_tokens)
2. ``docker login registry.gitlab.com`` enter your GitLab username and access token
3. go to your GitLab repository page and navigate to "Packages & Registries > Container Registry" and copy the registry url inside the build command
4. open the .env file and assign the copied value to ``GITLAB_CONTAINER_REGISTRY_URL``
5. ``./gustav build-push-gitlab-ci-image``
6. copy the image path after ``=> your image for .gitlab-ci.yml:`` and put it inside ``.gitlab-ci.yml`` under the key ``default.image``

### Determine where to deploy

Open ``.gitlab-ci.yml``, in the head section beneath ``default.before_script`` environment variables are defined.
You need to put your data in here in order to deploy something

- ``DEPLOY_SSH_ADDRESS`` shh user and address (username@remote-server.de)
- ``DEPLOY_PATH_SILVERSTRIPE`` absolute path to deployment root of silverstripe (/var/www/mydomain.de/httpdocs/silverstripe)
- ``DEPLOY_PATH_REACT`` absolute path to deployment root of react (/var/www/mydomain.de/httpdocs/react)
- ``DEPLOY_ENV_PATH_ADDITIONS`` this one is optional. If you need something to be appended to your path, add it here. Plesk for examples needs the path to the php executables (/var/www/vhosts/mydomain.de/.phpenv/shims:/opt/plesk/phpenv/bin)

### Deploy Scripts

The deployment commands are stored in several shell scripts. Starting with the ``ìnit.sh`` script,
which is executed first and makes all other scripts executable.

After that the scripts under ``deploy.script`` will be executed. First the Silverstripe backend (``silverstripe.sh``)
and then the React frontend (``react.sh``). Both scripts include a ``xxx_remote.sh`` script, which will be copied and
executed on the target server.

For now, ignore the ``combined.sh`` file.

### Test deployment without GitLab or deploy without GitLab

If you don't want to use GitLab CI/CD Pipelines or would like to test your deployment process without pushing to gitlab,
you can spin up the same image the pipelines would use with ``./gustav deploy``. This command will execute the scripts
inside the ``combined.sh`` file.

You can also specify which script to use, if you supply it as second parameter

``./gustav deploy react.sh``

By default, this will deploy **the stage of your CURRENT directory with all changes you made** to ``staging``.
You can deploy to ``production`` through submitting this keyword as second or if you're using specifying a script as third
parameter ``./gustav deploy react.sh production``

To connect via ssh and rsync to your target server, you need to put your ssh private key file named ``deploy-key``
in the root directory (same where the ``gustav`` script is located). If no key file is found, the script tries to use your local ``~/.ssh/ids_rsa`` key.

Also, you need your .env file for the target environment next to your local deployment .env file inside the ``code/silverstripe`` directory.
They should be named ``.env.production`` or ``.env.staging``.

### What is deployed?

Let's assume your ``DEPLOY_PATH_SILVERSTRIPE`` variable is set to ``/var/www/test.de/silverstripe`` than your deployed
folder structure will look like this.

└── /var/www/test.de/silverstripe\
&emsp;&emsp;├── staging (test site)\
&emsp;&emsp;├── deploy (only for deployment)\
&emsp;&emsp;│&emsp;&emsp;├── staging\
&emsp;&emsp;│&emsp;&emsp;└── production\
&emsp;&emsp;├── backup (each deployment creates a backup containing all files and the database)\
&emsp;&emsp;└── production (live site)

The deployment process is as follows

1. repo is prepared in docker container
2. dependencies and modules are installed there
3. node stuff is built with ``yarn build``
4. everything gets zipped and uploaded to your target server
5. unzipped on target server in deploy/xxx folder
6. backup of the production site is made (including database)
7. caches are cleared, dev/build gets executed
8. deploy/xxx folder becomes the new staging or production folder and your new version is live

That was a bit simplified, but roughly it goes like this. After each step, it is checked whether everything has worked,
and only then it continues. Should an error occur after dev/build. The previously made database backup is restored.

What is deployed at the end, is then with high confidence operable and your live site should not be broken ;)

## Reapply changed config / env / Dockerfile

1. changes inside ``.env`` file only require to shut down and restart your containers ``./gustav restart``
2. changes inside a ``Dockerfile`` require you to rebuild the image with ``./gustav build``
3. changes inside configuration or ini files (for example ``localhost.yml`` or ``core.ini``) require a ``down``and ``up`` command to restart your container or ``./gustav restart CONTAINER_NAME``

## Pitfalls and common problems

- MySql server address is not localhost but the name of the container (``mysql`)
- File(s) can't be edited nor deleted. Wrong user created / updated them, not devilbox (UID 1000)
- Sometimes the alpine gitlab-ci / local deploy image can't download composer / node dependencies. You need to restart your machine or docker to fix this ``sudo service docker restart``
- Node / yarn commands only work if the ``node-ss`` or ``node`` container is up and running. Those are only up in running if the projects have valid start scripts that will start a watch task or serve the react app
- Running multiple instances of GUSTAV is only possible if you change the ports for each project

## Todo

- [ ] Helper
  - [ ] Pull Assets from Remote (Option to override instead of append)
  - [ ] Push Assets to Remote (Option to override instead of append)
  - [ ] Pull Database from Remote
  - [ ] Push Database to Remote
- [ ] db import with .zip.sql files
- [ ] assets import with inline folder structures: '.' or './assets' or './public/assets'
- [ ] SSH Port for ssh and rsync other than the default
- [ ] Setup xDebug + Add HowTo to Readme
- [ ] Helper Script Autocompletion
- [ ] Helper Script Restart Docker Service
- [ ] Improve structure + add success checks for gustav script
- [ ] Improve structure of README
- [ ] Dependency Proxy https://docs.gitlab.com/ee/user/packages/dependency_proxy/ ?
- [ ] Keep the last two (or three?) backups not only the last
- [ ] Add caching in pipelines
