version: '3'

# --------
# Services
# --------
services:

  # ---------------------------------------------
  # Apache2 Webserver
  # https://github.com/devilbox/docker-apache-2.4
  # ---------------------------------------------
  apache:
    image: devilbox/apache-2.4:latest
    environment:
      TIMEZONE: Europe/Berlin
      NEW_UID: $HOST_UID
      NEW_GID: $HOST_GID
      PHP_FPM_ENABLE: 1
      PHP_FPM_SERVER_ADDR: php
      MAIN_VHOST_ENABLE: 1
      MAIN_VHOST_SSL_TYPE: both
      MAIN_VHOST_SSL_GEN: 1
      MAIN_VHOST_SSL_CN: localhost
      MAIN_VHOST_DOCROOT: 'htdocs/public'
    ports:
      - ${HTTP_PORT_SILVERSTRIPE}:80
      - ${HTTPS_PORT_SILVERSTRIPE}:443
    volumes:
      - ./code/silverstripe/:/var/www/default/htdocs/
    working_dir: /var/www/default/htdocs/
    depends_on:
      - php

  # -----------------------------------------------------------
  # PHP FPM
  # https://github.com/devilbox/docker-php-fpm#available-images
  # -----------------------------------------------------------
  php:
    build:
      context: ./build/php
      args:
        - PHP_VERSION=${PHP_VERSION}
    environment:
      NEW_GID: $HOST_GID
      NEW_UID: $HOST_UID
      COMPOSER_AUTH: '{"http-basic": {"${COMPOSER_REPOSITORY_URL}": {"username": "${COMPOSER_REPOSITORY_USERNAME}", "password": "${COMPOSER_REPOSITORY_PASSWORD}"}}, "gitlab-token": {"gitlab.com": "${COMPOSER_GITLAB_TOKEN}"}}'
    volumes:
      - ./code/silverstripe/:/var/www/default/htdocs/
      - ./.docker/php/ini:/etc/php-custom.d/
    working_dir: /var/www/default/htdocs/
    expose:
      - 9000
    depends_on:
      - mysql

  # ----------------------------------------
  # MariaDB Server
  # https://github.com/devilbox/docker-mysql
  # https://hub.docker.com/_/mariadb
  # ----------------------------------------
  mysql:
    image: devilbox/mysql:mariadb-${MARIA_DB_VERSION}
    restart: always
    environment:
      MARIADB_ROOT_PASSWORD: $MYSQL_ROOT_PASSWORD
      MARIADB_DATABASE: $MYSQL_DB
      MARIADB_USER: $MYSQL_USERNAME
      MARIADB_PASSWORD: $MYSQL_PASSWORD
    volumes:
      - mysql-data:/var/lib/mysql

  # ------------------------------------------------------------------------
  # PhpMyAdmin
  # https://hub.docker.com/r/phpmyadmin/phpmyadmin
  # https://docs.phpmyadmin.net/en/latest/setup.html#installing-using-docker
  # ------------------------------------------------------------------------
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    environment:
      PMA_HOST: mysql
    ports:
      - ${PHPMYADMIN_PORT}:80
    depends_on:
      - mysql

  # ----------------------------------------
  # MailHog - E-Mail-Testing
  # https://hub.docker.com/r/mailhog/mailhog
  # https://github.com/mailhog/MailHog
  # ----------------------------------------
  mailhog:
    image: mailhog/mailhog:latest
    logging:
      driver: syslog
    ports:
      - ${MAILHOG_PORT}:8025
      - "1025:1025"

  # -------------------------------------
  # Node
  # https://github.com/nodejs/docker-node
  # https://hub.docker.com/_/node
  #--------------------------------------

  node-ss: # silverstripe
    image: node:${NODE_VERSION}-alpine${ALPINE_VERSION}
    user: $HOST_UID
    environment:
      GITLAB_NPM_TOKEN: $GITLAB_NPM_TOKEN
    volumes:
      - ./code/silverstripe/:/var/www/default/htdocs/
    working_dir: /var/www/default/htdocs/
    command: sh -c 'yarn start' node

  node: # react
    image: node:${NODE_VERSION}-alpine${ALPINE_VERSION}
    user: $HOST_UID
    environment:
      GITLAB_NPM_TOKEN: $GITLAB_NPM_TOKEN
    volumes:
      - ./code/react/:/var/www/default/htdocs/
    working_dir: /var/www/default/htdocs/
    command: sh -c 'yarn start' node
    ports:
      - ${HTTP_PORT_REACT}:3000

# ------------------
# Persistent Volumes
# ------------------

volumes:
  mysql-data:
