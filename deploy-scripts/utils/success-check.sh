# $1 message | $2 == 1 => sub status
function success_check {
  if [[ $? -eq 0 ]]; then
    if [[ $2 ]]; then
      output_status "$1" 1
    else
      output_status "$1"
    fi
  else
    if [[ $2 ]]; then
      output_status "ERROR" 1
    else
      output_status "ERROR"
    fi
    exit 1
  fi
}