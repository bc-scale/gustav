#!/bin/bash

ROOT="$(dirname "${BASH_SOURCE[0]}")"

source "$ROOT/variables.sh"
source "$ROOT/output.sh"
source "$ROOT/success-check.sh"
source "$ROOT/ssh-rsync.sh"
source "$ROOT/create-remote-script.sh"