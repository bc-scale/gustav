SSH_KEY_ADDED=0

function add_ssh_key() {
  if [[ $SSH_KEY_ADDED -eq 1 ]]; then
    return
  fi

  SSH_KEY_ADDED=1

  output_header "ADD SSH KEY" 1
  eval "$(ssh-agent -s)"

  if [[ $DEPLOY_SSH_PRIVATE_KEY ]]; then
    echo "$DEPLOY_SSH_PRIVATE_KEY" | ssh-add -
  else
    ssh-add "$CI_PROJECT_DIR/deploy-key"
  fi

  success_check "ssh key added"
}

function ssh_client {
  add_ssh_key
  ssh "$DEPLOY_SSH_ADDRESS" "$@"
}

# $1 = file | $2 = path
function rsync_client {
  add_ssh_key
  rsync "$1" "$DEPLOY_SSH_ADDRESS:$2"
}

# $1 = filename | $2 = upload path
function file_upload {
  # upload project files to remote
  output_header "UPLOADING $1"

  # create upload path
  output_header "CREATING UPLOAD PATH" 1
  ssh_client "mkdir -p $2"
  success_check "upload path created: $2"

  # upload file
  output_header "STARTING UPLOAD" 1
  rsync_client "$1" "$2"
  success_check "upload successful to $2"
}

# $1 = script file | $2 command before script
function remote_script_exec {
  output_header "EXECUTING REMOTE SCRIPT"
  ssh_client "$2 bash -s" < "$1"
  success_check "remote script execution successful"
}