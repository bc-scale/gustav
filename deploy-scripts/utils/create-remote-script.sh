function create-remote-script {
  REMOTE_SCRIPT_NAME="$1"
  DEPLOY_SCRIPTS_DIR="$2"
  SPECIFIC_REMOTE_SCRIPT_NAME="$3"

  # combine multiple files into remote script
  echo '#!/bin/bash' > "$REMOTE_SCRIPT_NAME"
  echo "" >> "$REMOTE_SCRIPT_NAME"
  cat "$DEPLOY_SCRIPTS_DIR/utils/output.sh" >> "$REMOTE_SCRIPT_NAME"
  echo "" >> "$REMOTE_SCRIPT_NAME"
  cat "$DEPLOY_SCRIPTS_DIR/utils/success-check.sh" >> "$REMOTE_SCRIPT_NAME"
  echo "" >> "$REMOTE_SCRIPT_NAME"
  cat "$DEPLOY_SCRIPTS_DIR/utils/remote-deployment.sh" >> "$REMOTE_SCRIPT_NAME"
  echo "" >> "$REMOTE_SCRIPT_NAME"
  cat "$SPECIFIC_REMOTE_SCRIPT_NAME" >> "$REMOTE_SCRIPT_NAME"
  echo "" >> "$REMOTE_SCRIPT_NAME"
}