# filename containing zipped site
DEPLOY_ZIP="$CI_ENVIRONMENT_NAME-react.zip"
# path to this zip file
DEPLOY_ZIP_PATH="$DEPLOY_PATH_REACT/deploy/$DEPLOY_ZIP"
# path to deploy directory
DEPLOY_PATH="$DEPLOY_PATH_REACT/deploy/$CI_ENVIRONMENT_NAME"
# path to target directory where the site will be after deployment
TARGET_PATH="$DEPLOY_PATH_REACT/$CI_ENVIRONMENT_NAME"
# path of the old target in last deploy step when deployed version and live version will be switched
TARGET_PATH_MOVED="$DEPLOY_PATH_REACT/$CI_ENVIRONMENT_NAME-moved"

PATH_STAGING="$DEPLOY_PATH_REACT/staging"
PATH_PRODUCTION="$DEPLOY_PATH_REACT/production"
PATH_BACKUP="$DEPLOY_PATH_REACT/backup"

BACKUP_ZIP_FILE="$CI_ENVIRONMENT_NAME-backup.zip"
PATH_BACKUP_ZIP_FILE="$PATH_BACKUP/$BACKUP_ZIP_FILE"

if [[ $DEPLOY_ENV_PATH_ADDITIONS ]]; then
  export PATH=$DEPLOY_ENV_PATH_ADDITIONS:$PATH
fi

# delete former deployment
# ensure folder structure exists
# unzip files
prepare_deployment "$DEPLOY_PATH" "$PATH_STAGING" "$PATH_PRODUCTION" "$PATH_BACKUP" "$DEPLOY_ZIP" "$DEPLOY_ZIP_PATH"

# move unzipped files
move_deployed_files build "$DEPLOY_PATH" "$CI_ENVIRONMENT_NAME" "$DEPLOY_PATH_REACT"

# remove zip
clean_deployed_zip "$DEPLOY_ZIP_PATH" "$DEPLOY_ZIP"

# backup currently deployed site, override old backup
output_header "backup $CI_ENVIRONMENT_NAME site"

# already deployed site exists
if [[ -f "$TARGET_PATH/index.html" ]]; then
  # backup files
  output_header "starting files backup" 1
  zip "$PATH_BACKUP_ZIP_FILE" "$TARGET_PATH" -r
  success_check "zipping successful: $BACKUP_ZIP_FILE" 1
else
  output_status "no $CI_ENVIRONMENT_NAME site, skipping backup"
fi

moved_deployed_to_target "$CI_ENVIRONMENT_NAME" "$TARGET_PATH" "$TARGET_PATH_MOVED" "$DEPLOY_PATH"
